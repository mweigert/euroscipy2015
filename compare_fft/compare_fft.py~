import numpy as np
from numpy import *
import subprocess

from time import time
import gputools

def time_wrap(f):
    def func_to_time(dshape, Niter = 10, **kwargs):
        d = zeros(dshape,np.complex64)
        t = time()
        for _ in range(Niter):
            f(d, **kwargs)
        return (time()-t)/Niter

    return func_to_time

def time_wrap_g(f):
    def func_to_time(dshape, Niter = 10, **kwargs):
        d = zeros(dshape,np.complex64)
        d_g = gputools.OCLArray.from_array(d)
        #burn in
        f(d_g,**kwargs)
        
        gputools.get_device().queue.finish()

        t = time()
        for _ in range(Niter):
            f(d_g,**kwargs)
        
        gputools.get_device().queue.finish()

        return (time()-t)/Niter

    return func_to_time


@time_wrap
def fft_np(d):
    return np.fft.fftn(d)

@time_wrap
def fft_gpu_with_mem(d):
    return gputools.fft(d)

@time_wrap_g
def fft_gpu(d_g):
    return gputools.fft(d_g, inplace = True)

def fftw(N, single = True, measure = True):


    cmd = ["cpp/fftw_bench/fftw_bench_%s"%("single" if single else "multi"),"%s"%N]
    
    
    if measure:
        cmd.append("MEAS")
    
    print cmd
    
    ret = subprocess.check_output(cmd)
    return float(ret) 


if __name__ == "__main__":
    isPlot = False
    isPlot = True

    ns = 2**arange(5,9)
    ndim = 3

    res = []
    res.append(["np", [fft_np((n,)*ndim,2) for n in ns]])
    
    # res.append(["fftw_1_est", [fftw(n, single = True ) for n in ns]])
    # res.append(["fftw_32_est", [fftw(n, single = False ) for n in ns]])
    res.append(["fftw_1", [fftw(n, single = True, measure = False ) for n in ns]])
    res.append(["fftw_32",  [fftw(n, single = False, measure = False ) for n in ns]])
    
    res.append(["gpu_trans",  [fft_gpu_with_mem((n,)*ndim,6) for n in ns]])
    res.append(["gpu",  [fft_gpu((n,)*ndim,6) for n in ns]])
    
    
    if isPlot:
        import seaborn
        import pylab
        import pandas
        t = [r[1][-1] for r in res]
        names = [r[0] for r in res]
        
        d = pandas.Series(t,index=names)

        seaborn.barplot(d.index,1000*d.values,ci=None)
        
        pylab.ylabel("$t[ms]$")

        pylab.savefig("compare_fft.png")
