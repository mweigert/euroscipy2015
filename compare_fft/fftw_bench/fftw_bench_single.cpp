/* demonstrates 3d fft transforms - single threaded

usage: fftw_bench_single N

returns time elapsed

where the data size is then NxNxN    
   
 */

#include <fftw3.h>
#include <iostream>
#include <cstdlib>
#include <chrono>

using namespace std;

int main(int argc, char** argv ){

  int Niter = 3;

  int N = 256;
  bool is_measure = false;

  if (argc==2)
    N = atoi(argv[1]);
  
  if (argc==3)
    is_measure = true ;

  // cout<<"running with N = "<<N<<endl;
  fftw_complex *in, *out;
  fftw_plan p;

  in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N * N* N);

  out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N * N *N );
  
  for(int i=0;i<N*N*N;i++){
    in[i][0] = 1.;
    in[i][1] = 0.;
  }

  if (is_measure)
    p = fftw_plan_dft_3d(N,N,N, in, out, FFTW_FORWARD, FFTW_MEASURE);
  else
    p = fftw_plan_dft_3d(N,N,N, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
    


  auto start = std::chrono::system_clock::now();
  for (int i = 0;i<Niter;i++)
    fftw_execute(p); /* repeat as needed */

  auto end = std::chrono::system_clock::now();
  auto  elapsed = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
  float real_time = (1.e-6)/Niter*(float)elapsed.count();
  std::cout << real_time << '\n';

  fftw_destroy_plan(p);

  // cout<<out[0][0]<<" vs " << (N*N*N)<<endl;

  fftw_free(in); 
  fftw_free(out);

}
