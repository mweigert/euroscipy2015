/* demonstrates 3d fft transforms - multi threaded

usage: fftw_bench_multi N [MEAS] 

prints the time spend with only the fft to stdout
 
where the data size is then NxNxN    
   
 */

#include <fftw3.h>
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <chrono>

using namespace std;

int main(int argc, char** argv ){
  int N = 256;
  int Nthreads = 16;
  int Niter = 20;
  bool is_measure = false;

  if (argc==2)
    N = atoi(argv[1]);
  
  if (argc==3)
    is_measure = true ;


  fftwf_init_threads();

  fftwf_plan_with_nthreads(Nthreads);

  fftwf_complex *in, *out;
  fftwf_plan p;

  in = (fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex) * N * N* N);

  out = (fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex) * N * N *N );
  
  for(int i=0;i<N*N*N;i++){
    in[i][0] = 1.;
    in[i][1] = 0.;
  }
  
  if (is_measure)
    p = fftwf_plan_dft_3d(N,N,N, in, out, FFTW_FORWARD, FFTW_MEASURE);
  else
      p = fftwf_plan_dft_3d(N,N,N, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
  

  auto start = std::chrono::system_clock::now();

  for (int i = 0;i<Niter;i++)
    fftwf_execute(p); /* repeat as needed */

  auto end = std::chrono::system_clock::now();

  auto  elapsed = std::chrono::duration_cast<std::chrono::microseconds>(end - start);

  float real_time = (1.e-6)/Niter*(float)elapsed.count();

  std::cout << real_time << '\n';


  fftwf_destroy_plan(p);

  // cout<<out[0][0]<<" vs " << (N*N*N)<<endl;

  fftwf_free(in); 
  fftwf_free(out);

}
