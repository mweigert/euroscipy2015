import numpy as np
import scipy.fftpack as fp

from numpy import *
import subprocess

from time import time
import gputools

def time_wrap(f):
    def func_to_time(dshape, Niter = 10, **kwargs):
        d = zeros(dshape,np.complex64)
        t = time()
        for _ in range(Niter):
            f(d, **kwargs)
        return 1000.*(time()-t)/Niter

    return func_to_time

def time_wrap_g(f):
    def func_to_time(dshape, Niter = 10, **kwargs):
        d = zeros(dshape,np.complex64)
        d_g = gputools.OCLArray.from_array(d)
        #burn in
        f(d_g,**kwargs)
        
        gputools.get_device().queue.finish()

        t = time()
        for _ in range(Niter):
            f(d_g,**kwargs)
        
        gputools.get_device().queue.finish()

        return 1000.*(time()-t)/Niter

    return func_to_time


@time_wrap
def fft_np(d):
    return np.fft.fftn(d)

@time_wrap
def fft_sc(d):
    return fp.fftn(d)

@time_wrap
def fft_gpu_with_mem(d):
    return gputools.fft(d)

@time_wrap_g
def fft_gpu(d_g):
    return gputools.fft(d_g, inplace = True)

def fftw(N, single_thread = True, use_double_prec = False, measure = True):


    cmd = ["fftw_bench/build/fftw_bench_%s%s"%(
        ("single" if single_thread else "multi"),
        ("_f" if not use_double_prec else "")),
           "%s"%N]
    
    
    if measure:
        cmd.append("MEAS")
    
    print cmd
    
    ret = subprocess.check_output(cmd)
    return 1000.*float(ret) 


def absjoin(d,n):
    import os 
    return os.path.join(os.path.abspath(d),n)

if __name__ == "__main__":
    isPlot = False
    isPlot = True

    N = 256
    ndim = 3


    res = []
    res.append(["np", fft_np((N,)*ndim,4)])

    # res.append(["sc.fftpack", fft_sc((N,)*ndim,2)])

    # res.append(["fftw_1",  fftw(N, single_thread = True, 
    #                               use_double_prec = True,
    #                               measure = True )])
    
    res.append(["fftw_1_f",  fftw(N, single_thread = True, 
                                  use_double_prec = False,
                                  measure = True )])

    # res.append(["fftw_32",  fftw(N, single_thread = False, 
    #                               use_double_prec = True,
    #                               measure = True )])
    
    res.append(["fftw_32_f", fftw(N, single_thread = False, 
                                  use_double_prec = False,
                                  measure = True )])
    
    # res.append(["gpu_trans", fft_gpu_with_mem((N,)*ndim,6)])

    res.append(["gpu",  fft_gpu((N,)*ndim,6)])
    
    
    if isPlot:
        import seaborn
        import pylab
        import pandas
        times = [r[1] for r in res]
        names = [r[0] for r in res]
        
        d = pandas.Series(times,index=names)
        pylab.figure()
        pylab.clf()

        seaborn.barplot(d.index,d.values,ci=None, palette = "deep")

        pylab.title("fft 2c2 $%s$"%((N,)*ndim,))

        pylab.ylabel("$t[ms]$")
        pylab.yscale('log', nonposy='clip')

        for i,(name,t) in enumerate(res):
            pylab.annotate("%d ms"%t,(i,1.1*t),(i,1.1*t),ha ="center", va="bottom")

        pylab.savefig("../figs/plot_compare_fft_%s.png"%N)
