import numpy as np
from numpy import *

from scipy.signal import convolve2d
              
from skimage import color, data, restoration

from time import time
from gputools.deconv.deconv_rl import _deconv_rl_gpu_conv as _deconv_g
import gputools



def time_wrap(f):
    def func_to_time(dshape, hshape, Niter = 10, **kwargs):
        d = np.ones(dshape,np.float32)
        h = np.ones(hshape,np.float32)
        t = time()
        for _ in range(Niter):
            f(d,h, **kwargs);
        return 1000.*(time()-t)/Niter

    return func_to_time

def time_wrap_g(f):
    def func_to_time(dshape, hshape, Niter = 10, **kwargs):
        d = np.ones(dshape,np.float32)
        h = np.ones(hshape,np.float32)
   
        d_g = gputools.OCLArray.from_array(d)
        h_g = gputools.OCLArray.from_array(h)
        
        #burn in
        f(d_g,h_g,**kwargs)
        
        gputools.get_device().queue.finish()

        t = time()
        for _ in range(Niter):
            f(d_g,h_g,**kwargs)
        
        gputools.get_device().queue.finish()

        return 1000.*(time()-t)/Niter

    return func_to_time


@time_wrap
def deconv_skimage(d,h):
    return restoration.richardson_lucy(d, h, 10, clip = False)


@time_wrap_g
def deconv_gpu(d,h):
    return _deconv_g(d, h, 10)



if __name__ == "__main__":
    isPlot = False
    isPlot = True

    N = 256
    Nh = 13
    ndim = 2

    dshape = (N,)*ndim
    hshape = (Nh,)*ndim
    
    res = []
    
    res.append(["skimage", deconv_skimage(dshape,hshape,1)])

    res.append(["gpu", deconv_gpu(dshape,hshape,10)])
    
    
    if isPlot:
        import seaborn
        import pylab
        import pandas
        times = [r[1] for r in res]
        names = [r[0] for r in res]
        
        d = pandas.Series(times,index=names)
        pylab.figure()
        pylab.clf()

        seaborn.barplot(d.index,d.values,ci=None, palette = "deep")

        pylab.title("deconv $%s \otimes %s$"%(dshape,hshape))
        pylab.ylabel("$t[ms]$")
        pylab.yscale('log', nonposy='clip')

        for i,(name,t) in enumerate(res):
            pylab.annotate("%d ms"%t,(i,1.1*t),(i,1.1*t),ha ="center", va="bottom")
            
        pylab.savefig("../figs/plot_compare_deconv_%s_%s_%s.png"%(N,Nh,ndim))

