import numpy as np

import scipy.signal as signal


from time import time
import gputools

import scipy.ndimage.filters as ndimage


def time_wrap(f):
    def func_to_time(dshape, hshape, Niter = 10, **kwargs):
        d = np.ones(dshape,np.float32)
        h = np.ones(hshape,np.float32)
        t = time()
        for _ in range(Niter):
            f(d,h, **kwargs);
        return 1000.*(time()-t)/Niter

    return func_to_time

def time_wrap_g(f):
    def func_to_time(dshape, hshape, Niter = 10, **kwargs):
        d = np.ones(dshape,np.float32)
        h = np.ones(hshape,np.float32)
   
        d_g = gputools.OCLArray.from_array(d)
        h_g = gputools.OCLArray.from_array(h)
        
        #burn in
        f(d_g,h_g,**kwargs)
        
        gputools.get_device().queue.finish()

        t = time()
        for _ in range(Niter):
            f(d_g,h_g,**kwargs)
        
        gputools.get_device().queue.finish()

        return 1000.*(time()-t)/Niter

    return func_to_time


@time_wrap
def conv_scipy(d,h):
    return signal.convolve(d,h,mode="same")

@time_wrap
def conv_ndimage(d,h):
    return ndimage.convolve(d,h,mode="constant")

@time_wrap_g
def conv_gpu(d_g,h_g):
    return gputools.convolve(d_g,h_g)

    return float(ret) 


if __name__ == "__main__":
    isPlot = False
    isPlot = True

    N = 1024
    Nh = 13
    ndim = 2

    dshape = (N,)*ndim
    hshape = (Nh,)*ndim
    
    res = []
    
    res.append(["sc.signal", conv_scipy(dshape,hshape,1)])

    res.append(["sc.ndimage", conv_ndimage(dshape,hshape,1)])
    
    res.append(["gpu", conv_gpu(dshape,hshape,10)])
    


    # res.append(["gpu_trans", fft_gpu_with_mem((N,)*3,6)])
    # res.append(["gpu",  fft_gpu((N,)*3,6)])
    
    
    if isPlot:
        import seaborn
        import pylab
        import pandas
        times = [r[1] for r in res]
        names = [r[0] for r in res]
        
        d = pandas.Series(times,index=names)
        pylab.figure()
        pylab.clf()

        seaborn.barplot(d.index,d.values,ci=None, palette = "deep")

        pylab.title("conv $%s \otimes %s$"%(dshape,hshape))
        pylab.ylabel("$t[ms]$")
        pylab.yscale('log', nonposy='clip')

        for i,(name,t) in enumerate(res):
            pylab.annotate("%d ms"%t,(i,1.1*t),(i,1.1*t),ha ="center", va="bottom")

        
        pylab.savefig("../figs/plot_compare_conv_%s_%s_%s.png"%(N,Nh,ndim))
