import numpy as np
from numpy import *
import subprocess

from time import time
import gputools

def time_wrap(f):
    def func_to_time(dshape, Niter = 10, **kwargs):
        d = zeros(dshape,np.complex64)
        t = time()
        for _ in range(Niter):
            f(d, **kwargs)
        return (time()-t)/Niter

    return func_to_time

def time_wrap_g(f):
    def func_to_time(dshape, Niter = 10, **kwargs):
        d = zeros(dshape,np.complex64)
        d_g = gputools.OCLArray.from_array(d)
        #burn in
        f(d_g,**kwargs)
        
        gputools.get_device().queue.finish()

        t = time()
        for _ in range(Niter):
            f(d_g,**kwargs)
        
        gputools.get_device().queue.finish()

        return (time()-t)/Niter

    return func_to_time


@time_wrap
def fft_np(d):
    return np.fft.fftn(d)

@time_wrap
def fft_gpu_with_mem(d):
    return gputools.fft(d)

@time_wrap_g
def fft_gpu(d_g):
    return gputools.fft(d_g, inplace = True)

def fftw(N, single_thread = True, use_double_prec = False, measure = True):


    cmd = ["fftw_bench/build/fftw_bench_%s%s"%(
        ("single" if single_thread else "multi"),
        ("_f" if not use_double_prec else "")),
           "%s"%N]
    
    
    if measure:
        cmd.append("MEAS")
    
    print cmd
    
    ret = subprocess.check_output(cmd)
    return float(ret) 


if __name__ == "__main__":
    isPlot = False
    isPlot = True

    N = 256
    
    
    res = []
    res.append(["np", fft_np((N,)*3,2)])
    
    res.append(["fftw_1",  fftw(N, single_thread = True, 
                                  use_double_prec = True,
                                  measure = True )])
    
    res.append(["fftw_1_f",  fftw(N, single_thread = True, 
                                  use_double_prec = False,
                                  measure = True )])

    res.append(["fftw_32",  fftw(N, single_thread = False, 
                                  use_double_prec = True,
                                  measure = True )])
    
    res.append(["fftw_32_f", fftw(N, single_thread = False, 
                                  use_double_prec = False,
                                  measure = True )])
    
    res.append(["gpu_trans", fft_gpu_with_mem((N,)*3,6)])
    res.append(["gpu",  fft_gpu((N,)*3,6)])
    
    
    if isPlot:
        import seaborn
        import pylab
        import pandas
        times = [r[1] for r in res]
        names = [r[0] for r in res]
        
        d = pandas.Series(times,index=names)
        pylab.figure()
        pylab.clf()

        seaborn.barplot(d.index,1000*d.values,ci=None, palette = "deep")
        
        pylab.ylabel("$t[ms]$")

        pylab.savefig("plot_compare_fft.png")
