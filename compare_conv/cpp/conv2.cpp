#include <iostream>
#include <cstdlib>



int main(int argc, char *argv[])
{


  
  int N = 1024;
  int Nh = 5;

  if (argc>=3){
	N = atoi(argv[1]);
	Nh = atoi(argv[2]);
  }

  std::cout<<N<<" "<<Nh<<std::endl;
	

  float* img = new float[N*N];
  float kern[Nh*Nh];
  float* res = new float[N*N];


  for (int i = Nh/2; i < N-Nh/2; ++i)
	for (int j = Nh/2; j < N-Nh/2; ++j) {
	  float val = 0.f;
	  
	  for (int hi = -Nh/2; hi <= Nh/2; ++hi)
		for (int hj = -Nh/2; hj <= Nh/2; ++hj){
		  val += img[i+hi+N*(j+hj)]*kern[Nh/2+hi+Nh*(Nh/2+hj)];
		  //		  std::cout<<i<<" "<<hi<<" "<<hj<<std::endl;
		}
		  
	  
	
	  res[i+N*j] = val;
	}


  float summy = 0.f;
  
  for (int i =0; i < N; ++i)
	for (int j = 0; j < N; ++j)
	  summy += res[i+j*N];
  
  
  std::cout<<summy<<std::endl;

  delete [] img;
  delete [] res;

  return 0;
}
