import numpy as np
from numpy import *

from skimage.restoration import nl_means_denoising

from time import time
from gputools.denoise import nlm2
import gputools


def time_wrap(f):
    def func_to_time(dshape, fs, bs, Niter = 10, **kwargs):
        d = np.ones(dshape,np.float32)
        t = time()
        for _ in range(Niter):
            f(d,fs,bs, **kwargs);
        return 1000.*(time()-t)/Niter

    return func_to_time

    
@time_wrap
def denoise_skimage(d,fs,bs):
    return nl_means_denoising(d,fs,bs)


@time_wrap
def denoise_gpu(d,fs,bs):
    return nlm2(d, 10.,fs,bs)



if __name__ == "__main__":
    isPlot = False
    isPlot = True

    N = 1024
    ndim = 2
    fs = 4
    bs = 7
    
    dshape = (N,)*ndim
    
    res = []
    
    res.append(["skimage", denoise_skimage(dshape,fs,bs,2)])

    res.append(["gpu", denoise_gpu(dshape,fs,bs,2)])
    
    
    if isPlot:
        import seaborn
        import pylab
        import pandas
        times = [r[1] for r in res]
        names = [r[0] for r in res]
        
        d = pandas.Series(times,index=names)
        pylab.figure()
        pylab.clf()

        seaborn.barplot(d.index,d.values,ci=None, palette = "deep")

        pylab.title("denoise, nlm, %s,%s,%s)"%(dshape,fs,bs))
        pylab.ylabel("$t[ms]$")
        pylab.yscale('log', nonposy='clip')

        for i,(name,t) in enumerate(res):
            pylab.annotate("%d ms"%t,(i,1.1*t),(i,1.1*t),ha ="center", va="bottom")
            
        pylab.savefig("../figs/plot_compare_denoise_%s_%s_%s.png"%(N,fs,bs))

